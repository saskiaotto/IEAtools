---
output: github_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<!-- README.md is generated from README.Rmd. Please edit that file -->

# IEAtools   
<!-- <img src="tools/images/rquiz_logo.png" align="right" width="100" height="112" /> -->

This R package provides functions for conducting Integrated Ecosystem Assessments (IEA)
